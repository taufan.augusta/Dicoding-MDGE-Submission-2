﻿using UnityEngine;

public class OrangeBlockActivator : MonoBehaviour {

	private MeshCollider colliderCache;

	private void Start() 
	{
		colliderCache = GetComponent<MeshCollider>();
	}
	
	void Update () 
	{
		bool absolutePosition = AbsoulutePosition.inOrigin;
		if(!absolutePosition)
			colliderCache.isTrigger = true;
		else
			colliderCache.isTrigger = false;


	}
}
