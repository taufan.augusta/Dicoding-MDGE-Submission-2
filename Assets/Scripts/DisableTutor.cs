﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class DisableTutor : MonoBehaviour 
{
	private void Update () 
	{
		if(CrossPlatformInputManager.GetButtonDown("Warp"))
			Destroy(this.gameObject);
	}
}
