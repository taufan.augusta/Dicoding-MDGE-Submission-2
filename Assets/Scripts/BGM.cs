﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGM : MonoBehaviour 
{
	private void Awake() 
	{
		GameObject[] BGM = GameObject.FindGameObjectsWithTag("Music");
		if(BGM.Length > 1)
			Destroy(this.gameObject);

		DontDestroyOnLoad(this.gameObject);		
	}
}
